﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.IO;
using System.Net;
using System.Reflection;
using MHLab.PATCH;
using MHLab.PATCH.Debugging;
using MHLab.PATCH.Settings;
using MHLab.PATCH.Utilities;
using MHLab.PATCH.Install;
using Microsoft.Win32;
using System.Text.RegularExpressions;

namespace PATCH_Launcher_WPF    
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Private fields
        LauncherManager m_launcher;
        InstallManager m_installer;
        Thread checkUpdates;
        #endregion

        public MainWindow()
        {
            InitializeComponent();

            AppDomain.CurrentDomain.UnhandledException += (sender, e) => FatalExceptionObject(e.ExceptionObject);

            m_launcher = new LauncherManager();
            m_launcher.LoadSettings();
            m_launcher.SetOnSetMainProgressBarAction(OnSetMainProgressBar);
            m_launcher.SetOnSetDetailProgressBarAction(OnSetDetailProgressBar);
            m_launcher.SetOnIncreaseMainProgressBarAction(OnIncreaseMainProgressBar);
            m_launcher.SetOnIncreaseDetailProgressBarAction(OnIncreaseDetailProgressBar);
            m_launcher.SetOnLogAction(OnLog);
            m_launcher.SetOnErrorAction(OnError);
            m_launcher.SetOnFatalErrorAction(OnFatalError);
            m_launcher.SetOnTaskStartedAction(OnTaskStarted);
            m_launcher.SetOnTaskCompletedAction(OnTaskCompleted);
            m_launcher.SetOnDownloadProgressAction(OnDownloadProgress);
            m_launcher.SetOnDownloadCompletedAction(OnDownloadCompleted);

            m_installer = new InstallManager();
            m_installer.LoadSettings();
            m_installer.SetOnSetMainProgressBarAction(OnSetMainProgressBar);
            m_installer.SetOnSetDetailProgressBarAction(OnSetDetailProgressBar);
            m_installer.SetOnIncreaseMainProgressBarAction(OnIncreaseMainProgressBar);
            m_installer.SetOnIncreaseDetailProgressBarAction(OnIncreaseDetailProgressBar);
            m_installer.SetOnLogAction(OnLog);
            m_installer.SetOnErrorAction(OnError);
            m_installer.SetOnFatalErrorAction(OnFatalError);
            m_installer.SetOnTaskStartedAction(OnTaskStarted);
            m_installer.SetOnTaskCompletedAction(OnInstallerTaskCompleted);
            m_installer.SetOnDownloadProgressAction(OnDownloadProgress);
            m_installer.SetOnDownloadCompletedAction(OnDownloadCompleted);

            this.LaunchButton.IsEnabled = false;
            this.BackButton.Visibility = Visibility.Hidden;
            this.ToolsContainer.Visibility = Visibility.Hidden;
            this.OptionsBackButton.Visibility = Visibility.Hidden;
            this.IsDirtyBackdrop.Visibility = Visibility.Hidden;
            this.RegisterBackButton.Visibility = Visibility.Hidden;

            String appId = "5b4e039528f84d5f46bad21b";
            String appVersion = m_launcher.GetCurrentVersion();

            /**
             * Initialize the service
             */
            Caphyon.AdvancedAnalytics lAdvancedAnalytics = new Caphyon.AdvancedAnalytics();
            lAdvancedAnalytics.Start(appId, appVersion);

            /**
             * Check to see if tracking is enabled.
             * This must be done once at startup. The user option needs to persist.
             */
            String keyName = "HKEY_CURRENT_USER\\SOFTWARE\\Caphyon\\LaunchAnalytics\\"
                             + appId + "\\" + appVersion;
            bool regValue = false;

            try
            {
                regValue = (string)Registry.GetValue(keyName, "SWG Launcher", false) == "True" ? true : false;
            }
            catch (Exception)
            {
                Console.Write("There is no registry yet. Assuming false.");
            }

            if (regValue == false)
            {
                lAdvancedAnalytics.Enable(true);
                Registry.SetValue(keyName, "SWG Launcher", true);
            }
        }

        #region Callbacks for New version
        public void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= TextBox_GotFocus;
        }

        void OnSetMainProgressBar(int min, int max)
        {
            this.MainProgressBar.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                this.MainProgressBar.Maximum = max;
                this.MainProgressBar.Minimum = min;

                this.MainProgressBar.Value = (min > 0) ? min : 0;
            }));
        }

        void OnSetDetailProgressBar(int min, int max)
        {
            this.ProgressBar.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                this.ProgressBar.Maximum = max;
                this.ProgressBar.Minimum = min;

                this.ProgressBar.Value = (min > 0) ? min : 0;
            }));
        }

        void OnIncreaseMainProgressBar()
        {
            this.MainProgressBar.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                if (this.MainProgressBar.Value < this.MainProgressBar.Maximum)
                    this.MainProgressBar.Value += 1;
            }));
        }

        void OnIncreaseDetailProgressBar()
        {
            this.ProgressBar.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                if (this.ProgressBar.Value < this.ProgressBar.Maximum)
                    this.ProgressBar.Value += 1;
            }));
        }

        void OnLog(string main, string detail)
        {
            this.MainDebug.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                this.MainDebug.Content = main;
                this.DetailsDebug.Content = detail;
            }));
        }

        void OnError(string main, string detail, Exception e)
        {
            try
            {
                this.MainDebug.Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    this.MainDebug.Content = main;
                    this.DetailsDebug.Content = detail;
                    Debugger.Log(e.Message);
                }));
            }
            catch(Exception Error)
            {
                Debugger.Log(Error.Message);
            }
        }

        void OnFatalError(string main, string detail, Exception e)
        {
            try
            {
                this.MainDebug.Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    this.MainDebug.Content = main;
                    this.DetailsDebug.Content = detail;
                    Debugger.Log(e.Message);
                    this.checkUpdates.Abort();

                    this.ForceRepairButton.IsEnabled = true;
                }));
            }
            catch(Exception Error)
            {
                Debugger.Log(Error.Message);
                this.LaunchButton.IsEnabled = true;
            }
        }

        void OnTaskStarted(string message)
        {
            this.MainDebug.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                this.MainDebug.Content = message;
                this.DetailsDebug.Content = "";
                this.ForceRepairButton.IsEnabled = false;
            }));
        }

        void OnTaskCompleted(string message)
        {
            this.MainDebug.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                this.CurrentBuildLabel.Content = "Build: v" + m_launcher.GetCurrentVersion();
                this.MainDebug.Content = message;
                this.DetailsDebug.Content = "";

                if (!m_launcher.IsDirty())
                {
                    this.LaunchButton.IsEnabled = true;
                    this.ForceRepairButton.IsEnabled = true;
                }
                else
                {
                    this.IsDirtyBackdrop.Visibility = Visibility.Visible;
                }
            }));
        }

        void OnInstallerTaskCompleted(string message)
        {
            this.MainDebug.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                this.CurrentBuildLabel.Content = "Build: v" + m_launcher.GetCurrentVersion();
                this.MainDebug.Content = message;
                this.DetailsDebug.Content = "";

                if (!m_launcher.IsDirty())
                {
                    if (!SettingsManager.ENABLE_PATCHER)
                    {
                        this.LaunchButton.IsEnabled = true;
                        this.ForceRepairButton.IsEnabled = true;
                    }
                }
                else
                {
                    this.IsDirtyBackdrop.Visibility = Visibility.Visible;
                }
            }));
        }

        DateTime _lastTime = DateTime.UtcNow;
        long _lastSize = 0;
        int _downloadSpeed = 0;

        void OnDownloadProgress(long currentFileSize, long totalFileSize, int percentageCompleted)
        {
            if (_lastTime.AddSeconds(1) <= DateTime.UtcNow)
            {
                _downloadSpeed = (int)((currentFileSize - _lastSize) / (DateTime.UtcNow - _lastTime).TotalSeconds);
                _lastSize = currentFileSize;
                _lastTime = DateTime.UtcNow;
            }

            this.MainDebug.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                try
                {
                    this.ProgressBar.Value = percentageCompleted;
                }
                catch { }
                this.DownloadProgressLabel.Content = percentageCompleted + "% - (" + Utility.FormatSizeBinary(currentFileSize, 2) + "/" + Utility.FormatSizeBinary(totalFileSize, 2) + ") @ " + Utility.FormatSizeBinary(_downloadSpeed, 2) + "/s";
            }));
        }

        void OnDownloadCompleted()
        {
            this.MainDebug.Dispatcher.BeginInvoke(new Action(delegate ()
            {
                this.DownloadProgressLabel.Content = "";
            }));
        }
        #endregion

        #region Private methods
        private void Navigate(string url)
        {
            try
            {
                this.NewsBrowser.Navigate(new Uri(url));
            }
            catch(Exception Error)
            {
                Debugger.Log(Error.Message);
            }
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            try
            {
                this.CurrentBuildLabel.Content = "Build: v" + m_launcher.GetCurrentVersion();

                Navigate(SettingsManager.NEWS_URL);
              
                StartCheckForUpdatesThread();
            }
            catch (Exception Error)
            {
                Debugger.Log(Error.Message);
            }
        }

        void StartCheckForUpdatesThread()
        {
            this.checkUpdates = new Thread(new ThreadStart(CheckForUpdates));
            this.checkUpdates.IsBackground = true;
            this.checkUpdates.Name = "Patch thread";
            try
            {
                this.checkUpdates.Start();
                LaunchUpdater();
            }
            catch (Exception Error)
            {
                Debugger.Log(Error.Message);
            }
        }

        void LaunchUpdater()
        {
            System.Diagnostics.Process.Start(SettingsManager.APP_PATH + System.IO.Path.DirectorySeparatorChar.ToString() + "The SWG Source Launcher Updater.exe");
        }

        void CheckForUpdates()
        {
            try
            {
                if (!SettingsManager.INSTALL_IN_LOCAL_PATH)
                {
                    SettingsManager.APP_PATH = m_installer.GetInstallationPath();
                    SettingsManager.RegeneratePaths();
                }

                bool isToInstall = m_installer.IsToInstall();
                InstallationState installStatus;
                InstallationState installPatcherStatus;

                if (SettingsManager.ENABLE_INSTALLER)
                {
                    if (isToInstall)
                    {
                        try
                        {
                            installStatus = m_installer.Install();


                            if (SettingsManager.INSTALL_PATCHER && installStatus == InstallationState.SUCCESS && !SettingsManager.INSTALL_IN_LOCAL_PATH)
                                installPatcherStatus = m_installer.InstallPatcher();
                            else
                                installPatcherStatus = InstallationState.SUCCESS;

                            if (SettingsManager.CREATE_DESKTOP_SHORTCUT && installStatus == InstallationState.SUCCESS && installPatcherStatus == InstallationState.SUCCESS && SettingsManager.INSTALL_PATCHER)
                            {
                                //Debugger.Log ("Shortcut creation linked to launcher");
                                m_installer.CreateShortcut();
                            }
                            else if (SettingsManager.CREATE_DESKTOP_SHORTCUT && (installPatcherStatus == InstallationState.FAILED || !SettingsManager.INSTALL_PATCHER))
                            {
                                //Debugger.Log ("Shortcut creation linked to game");
                                if (SettingsManager.ENABLE_PATCHER)
                                    m_installer.CreateShortcut();
                                else
                                    m_installer.CreateShortcut(false);
                            }
                        }
                        catch (Exception Error)
                        {
                            Debugger.Log(Error.Message);
                        }
                    }
                }

                if (SettingsManager.ENABLE_REPAIRER)
                {
                    if (!isToInstall)
                    {
                        m_installer.Repair();
                    }
                }

                if (SettingsManager.ENABLE_PATCHER)
                    try
                    {
                        CheckForPatches();
                    }
                    catch (Exception Error)
                    {
                        Debugger.Log(Error.Message);
                        this.LaunchButton.IsEnabled = true;
                    }

                if (!SettingsManager.INSTALL_IN_LOCAL_PATH)
                {
                    SettingsManager.APP_PATH = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    SettingsManager.RegeneratePaths();
                }
            }
            catch (Exception Error)
            {
                Debugger.Log(Error.Message);
            }

           
        }

        void CheckForPatches()
        {
            m_launcher.CheckForUpdates();
        }

        private void CloseButton_MouseDown(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void IconButton_MouseDown(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void LaunchButton_Click(object sender, RoutedEventArgs e)
        {
           
            
            string username = Username.Text;
            string strFile = File.ReadAllText(@"user.cfg");
            strFile = Regex.Replace(strFile, @"(.*?)loginClientID=\b([^\s]+)", "     "+"loginClientID="+username);
            File.WriteAllText(@"user.cfg", strFile);

            System.Diagnostics.Process.Start(SettingsManager.LAUNCH_APP, (SettingsManager.USE_RAW_LAUNCH_ARG) ? SettingsManager.LAUNCH_ARG : SettingsManager.LAUNCH_COMMAND);
            
            //Application.Current.Shutdown();
        }



    


        private void RestartButton_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = SettingsManager.LAUNCHER_NAME;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.Verb = "runas";
            process.Start();
            System.Windows.Application.Current.Shutdown();
        }

        private void DragMoveMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (e.ChangedButton == MouseButton.Left)
                    this.DragMove();
            }
            catch
            {

            }
        }

        private void PatchnotesButton_Click(object sender, RoutedEventArgs e)
        {
            Navigate(SettingsManager.PATCHNOTES_URL);
            this.PatchnotesButton.Visibility = Visibility.Hidden;
            this.BackButton.Visibility = Visibility.Visible;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Navigate(SettingsManager.NEWS_URL);
            this.PatchnotesButton.Visibility = Visibility.Visible;
            this.BackButton.Visibility = Visibility.Hidden;
        }

        private void OptionsButton_Click(object sender, RoutedEventArgs e)
        {
            this.PatchnotesButton.Visibility = Visibility.Hidden;
            this.BackButton.Visibility = Visibility.Hidden;
            this.OptionsButton.Visibility = Visibility.Hidden;
            this.NewsBrowser.Visibility = Visibility.Hidden;
            this.OptionsBackButton.Visibility = Visibility.Visible;
            this.ToolsContainer.Visibility = Visibility.Visible;
        }

        private void OptionsBackButton_Click(object sender, RoutedEventArgs e)
        {
            Navigate(SettingsManager.NEWS_URL);
            this.PatchnotesButton.Visibility = Visibility.Visible;
            this.BackButton.Visibility = Visibility.Hidden;
            this.OptionsButton.Visibility = Visibility.Visible;
            this.NewsBrowser.Visibility = Visibility.Visible;
            this.OptionsBackButton.Visibility = Visibility.Hidden;
            this.ToolsContainer.Visibility = Visibility.Hidden;
        }

        private void ForceRepairButton_Click(object sender, RoutedEventArgs e)
        {
            FileManager.DeleteFile(SettingsManager.VERSION_FILE_LOCAL_PATH);
            StartCheckForUpdatesThread();
        }
        #endregion

        #region Unhandled Exception
        void FatalExceptionObject(object exceptionObject)
        {
            try
            {
                Exception exception = exceptionObject as Exception;
                if (exception == null)
                {
                    exception = new NotSupportedException(
                      "Unhandled exception doesn't derive from System.Exception: "
                       + exceptionObject.ToString()
                    );
                }
                HandleException(exception);
            }
            catch(Exception Error)
            {
                Debugger.Log(Error.Message);
            }
        }

        void HandleException(Exception e)
        {
            try
            {
                OnFatalError("An unhandled error occurred!", "Please, restart your Launcher!", e);
            }
            catch(Exception Error)
            {
                Debugger.Log(Error.Message);
            }
        }
        #endregion

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {


            try
            {
                this.NewsBrowser.Navigate("https://auth.nomercyever.com/secure/registerclient.php");
                this.RegisterButton.Visibility = Visibility.Hidden;
                this.RegisterBackButton.Visibility = Visibility.Visible;


            }
            catch
            {

            }
        }

        private void RegisterBackButton_Click(object sender, RoutedEventArgs e)
        {


            try
            {
                Navigate(SettingsManager.NEWS_URL);
                this.RegisterBackButton.Visibility = Visibility.Hidden;
                this.RegisterButton.Visibility = Visibility.Visible;
            }
            catch
            {

            }

        }

        private void GameSetup_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.FileName = (SettingsManager.APP_PATH + System.IO.Path.DirectorySeparatorChar.ToString() + "SwgClientSetup.exe");
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.Verb = "runas";
                process.Start();
                //System.Diagnostics.Process.Start(SettingsManager.APP_PATH + System.IO.Path.DirectorySeparatorChar.ToString() + "SwgClientSetup.exe");
            }
            catch (Exception Error)
            {
                Debugger.Log(Error.Message);
            }
        }
    }
}
