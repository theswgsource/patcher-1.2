# Launcher/Patcher Repository

This is a client Installer/Launcher/Updater that will install the game in the directory it is launched from and keep it up to date based on an online check.  Current pulls come from a CDN hosted via Witt. Please contact with any questions.

# Usage

If you are just looking to use the core application for installing and launching then run the setup program in the /installer folder.

If you are looking to see just the Launcher application, what is installed via the installer, the current compiled version is in /bin.

If you are looking to work on the code itself then grab all the files in the /src foler.  All files designed with Visual Studion 2017 and .NET 4.5.2

# Guides

Guides on how the whole system works from start to finish will be completed at a later date along with the uploading of the patch making application itself. 

# Useful Changes Appreciated

If you make any useful changes or improvements, put in a pull request/merge request and I'll merge it in, and you'll become a contributor!

